angular.module('calendarDemoApp', ['ui.rCalendar']);

angular.module('calendarDemoApp').controller('CalendarDemoCtrl', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Họp rà soát nhiệm vụ phòng, chống thiên tai khu vực miền Trung và Tây Nguyên Năm 2019",
                noidung: "Họp rà soát nhiệm vụ phòng, chống thiên tai khu vực miền Trung và Tây Nguyên Năm 2019",
                thamdu: "",
                diadiem: "Phòng họp A4",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 23, 59, 59, 0),
                allDay: false
            },
            {
                title: "Họp đề án ' Rà soát, đánh giá toàn diện, đề xuát mô hình cơ quan quản lý nhà nước về phòng chống thiên tai'",
                noidung: "Họp đề án '' Rà soát, đánh giá toàn diện, đề xuát mô hình cơ quan quản lý nhà nước về phòng chống thiên tai'",
                thamdu: "",
                diadiem: "Phòng họp A4",
                startTime: new Date(2018, 8, 21, 14, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 16, 0, 0, 0),
                allDay: false
            },
            {
                title: "Họp về hỗ trợ thiệt hại do bão số 4 và mưa lũ, ngập lụt, lũ quét, sạt lở đất tại các tỉnh Bắc Bộ và Bắc Trung Bộ từ ngày 27/8-31/8/2018",
                noidung: "Họp về hỗ trợ thiệt hại do bão số 4 và mưa lũ, ngập lụt, lũ quét, sạt lở đất tại các tỉnh Bắc Bộ và Bắc Trung Bộ từ ngày 27/8-31/8/2018",
                thamdu: "",
                diadiem: "Phòng họp A4",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 13, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end bộ trưởng  ----->

angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr1', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Dự họp theo lịch Tổng cục trưởng",
                noidung: "Dự họp theo lịch Tổng cục trưởng",
                thamdu: "",
                diadiem: "",
                startTime: new Date(2018, 8, 21, 8, 30, 0, 0),
                endTime: new Date(2018, 8, 21, 9, 30, 0, 0),
                allDay: false
            },
            {
                title: "Làm việc với Công ty Takino Filter Nhật Bản",
                noidung: "Làm việc với Công ty Takino Filter Nhật Bản",
                thamdu: "Vụ HTQT / Vụ QLĐĐ, TTCS",
                diadiem: "P206",
                startTime: new Date(2018, 8, 21, 8, 30, 0, 0),
                endTime: new Date(2018, 8, 21, 11, 0, 0, 0),
                allDay: false
            },
            {
                title: "Dự họp theo lịch Bộ trưởng",
                noidung: "Dự họp theo lịch Bộ trưởng",
                thamdu: "Chị Nga, Vụ HTQT",
                diadiem: "Lotte",
                startTime: new Date(2018, 8, 21, 8, 30, 0, 0),
                endTime: new Date(2018, 8, 21, 11, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end thứ trưởng 1  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr2', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Họp rà soát chuẩn bị PCTT miền Trung",
                noidung: "Họp rà soát chuẩn bị PCTT miền Trung",
                thamdu: "",
                diadiem: "A4",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 23, 59, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

